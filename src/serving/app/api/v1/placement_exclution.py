from sqlalchemy import create_engine

from cerberus import Validator
from cerberus.errors import ValidationError
import numpy as np
import pandas as pd
from src.serving.app import log
from src.serving.app.api.common import BaseResource
from src.serving.app.errors import InvalidParameterError
from src.serving.app.maths.outlier_removal import OutlierRemoval

LOG = log.get_logger()

FIELDS = {
    'start_date': {
        'type': 'string',
        'required': False
    },
    'end_date': {
        'type': 'string',
        'required': False
    },
    'device': {
        'type': 'string',
        'required': False
    },
    'network': {
        'type': 'string',
        'required': False
    },
    'metric': {
        'type': 'string',
        'required': True
    }
}


def validate_request(req, res, resouce, params):
    schema = {
        'start_date': FIELDS['start_date'],
        'end_date': FIELDS['end_date'],
        'device': FIELDS['device'],
        'network': FIELDS['network'],
        'metric': FIELDS['metric']
    }

    v = Validator(schema)

    try:
        if not v.validate(req.context['data']):
            raise InvalidParameterError(v.errors)

    except ValidationError:
        raise InvalidParameterError('Invalid Request %s' % req.context)


class Collection(BaseResource):

    def on_post(self, req, res):
        session = req.context['session']
        user_req = req.context['data']
        if user_req:
            start_date = user_req['start_date']
            end_date = user_req['end_date']
            device = user_req['device']
            network = user_req['network']
            metric = user_req['metric']
            output_df = OutlierRemoval.run_pipe_line(start_date, end_date, device, network, metric)
            self.on_success(res, output_df.to_dict('records'))
        else:
            raise InvalidParameterError(req.context['data'])