from sqlalchemy import create_engine
import numpy as np
import pandas as pd
from src.serving.app.errors import UnsupportedParameterError, DatabaseError, ERR_DATABASE_QUERY
from src.serving.app.config import DATABASE_URL


class OutlierRemoval:

    @staticmethod
    def run_pipe_line(start_date, end_date, device, network, metric):
        """
        :param start_date:
        :param end_date:
        :param device:
        :param network:
        :param metric:
        :return:
        """
        input_df = OutlierRemoval.__prepare_data(start_date, end_date, device, network, metric)
        output_df = OutlierRemoval.__process_outlier(input_df, metric)
        return output_df

    @staticmethod
    def __process_outlier(input_df, metric):
        """
        Process Outlier for
        :param input_df:
        :param metric:
        :return:
        """
        output_df = input_df
        metrics = np.array(input_df[metric])
        output_df['label'] = output_df[metric].apply(lambda x: OutlierRemoval.__iqr_map(x, metrics))
        return output_df

    @staticmethod
    def __prepare_data(start_date, end_date, device, network, metric):
        """
        Prepare data : Step 1:
        :param start_date:
        :param end_date:
        :param device:
        :param network:
        :param metric:
        :return:
        """
        supported_metrics = {'AverageCpm', 'AverageCpc', 'Clicks', 'Impressions', 'CostPerConversion'}
        try:
            conn = create_engine(DATABASE_URL)
            if metric in supported_metrics:
                sql = OutlierRemoval.__build_sql_query(start_date, end_date, device, network, supported_metrics)
                placement_df = pd.read_sql(sql, con=conn)
                conn.dispose()
                return placement_df
            else:
                raise UnsupportedParameterError(metric)

        except UnsupportedParameterError as error:
            raise UnsupportedParameterError(error.description)
        except Exception as error:
            raise DatabaseError(ERR_DATABASE_QUERY, str(error),
                                [start_date, end_date, device, network])
        finally:
            conn.dispose()

    @staticmethod
    def __iqr_map(y, data_points):
        """
        map function that apply iqr rule to a cell. Compare cell with the qualifier.
        :param y:
        :param data_points:
        :return:
        """
        sorted(data_points)
        label = 'Normal'
        q1, q3 = np.percentile(data_points, [25, 75])
        iqr = q3 - q1
        lower_bound = q1 - 1.5 * iqr
        upper_bound = q3 + 1.5 * iqr
        if y < lower_bound:
            label = 'Bad'
        if y > upper_bound:
            label = 'Outstanding'
        return label

    def __z_score(self, data_points):
        """
        Z_score rule
        :param data_points:
        :return:
        """
        outlier = []
        threshold = 3
        mean = np.mean(data_points)
        std = np.std(data_points)
        for y in data_points:
            z_score = (y - mean) / std
            if np.abs(z_score) > threshold:
                outlier.append(y)
        return outlier

    @staticmethod
    def __build_sql_query(start_date, end_date, device, network, supported_metrics):
        """
        Build static sql query. Should be refactored to get more flexibility
        :param start_date:
        :param end_date:
        :param device:
        :param network:
        :param supported_metrics:
        :return:
        """
        sql = "SELECT CampaignId, Id, Status, " + ','.join(supported_metrics) + \
              " FROM placement_performance_report"
        condition = []
        if start_date and end_date:
            condition.append(
                " placement_performance_report.DateTime >= '" + start_date +
                "' AND placement_performance_report.DateTime <= '" + end_date + "' ")
        if network:
            condition.append(" placement_performance_report.AdNetworkType1 = '" + network + "' ")
        if device:
            condition.append(" placement_performance_report.Device = '" + device + "' ")
        if len(condition) > 0:
            sql = sql + " WHERE " + " AND ".join(str(x) for x in condition)
        return sql
