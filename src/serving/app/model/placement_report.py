from sqlalchemy import Column
from sqlalchemy import String, Integer, LargeBinary, Boolean, Float, DateTime, and_
from sqlalchemy.dialects.postgresql import JSONB

from src.serving.app.model import Base
from src.serving.app.config import UUID_LEN
from src.serving.app.utils import alchemy


class PlacementReport(Base):
    __tablename__ = 'placement_performance_report'

    AccountDescriptiveName = Column(String())
    AdGroupId = Column(String())
    BaseAdGroupId = Column(String())
    CampaignId = Column(String(),primary_key=True)
    Criteria = Column(String())
    CriteriaDestinationUrl = Column(String())
    Device = Column(String())
    DisplayName = Column(String())
    Id = Column(String(),primary_key=True)
    FinalAppUrls = Column(String())
    ExternalCustomerId = Column(String())
    FinalMobileUrls = Column(String())
    FinalUrls = Column(String())
    IsNegative = Column(Boolean())
    IsRestrict = Column(Boolean())
    Status = Column(String())
    DateTime = Column(DateTime(),primary_key=True)
    AdNetworkType1 = Column(String())
    AdNetworkType2 = Column(String())
    AverageCost = Column(Float())
    AverageCpc = Column(Float())
    AverageCpm = Column(Float())
    Clicks = Column(Integer())
    ConversionRate = Column(Float())
    Ctr = Column(Float())
    Impressions = Column(Integer())

    def __init__(self, AccountDescriptiveName, AdGroupId, BaseAdGroupId, CampaignId, Criteria, CriteriaDestinationUrl,
                 Device, DisplayName, Id, FinalAppUrls, ExternalCustomerId, FinalMobileUrls, FinalUrls, IsNegative,
                 IsRestrict, Status, DateTime, AdNetworkType1, AdNetworkType2, AverageCost, AverageCpc, AverageCpm,
                 Clicks,
                 ConversionRate, Ctr, Impressions):
        self.AccountDescriptiveName = AccountDescriptiveName
        self.AdGroupId = AdGroupId
        self.BaseAdGroupId = BaseAdGroupId
        self.CampaignId = CampaignId
        self.Criteria = Criteria
        self.CriteriaDestinationUrl = CriteriaDestinationUrl
        self.Device = Device
        self.DisplayName = DisplayName
        self.Id = Id
        self.FinalAppUrls = FinalAppUrls
        self.ExternalCustomerId = ExternalCustomerId
        self.FinalMobileUrls = FinalMobileUrls
        self.FinalUrls = FinalUrls
        self.IsNegative = IsNegative
        self.IsRestrict = IsRestrict
        self.Status = Status
        self.DateTime = DateTime
        self.AdNetworkType1 = AdNetworkType1
        self.AdNetworkType2 = AdNetworkType2
        self.AverageCost = AverageCost
        self.AverageCpc = AverageCpc
        self.AverageCpm = AverageCpm
        self.ConversionRate = ConversionRate
        self.Ctr = Ctr
        self.Impressions = Impressions

    def __repr__(self):
        return "Record : PlacementId {0} - CampaignId {1} - DateTime {2}".format(self.Id, self.CampaignId,
                                                                                 self.DateTime)

    @classmethod
    def filter_by(cls,session,start_date,end_date,device,network):
        criteria = {}
        if device: criteria['Device'] = device
        if network:
            criteria['AdNetworkType1'] = network
        if start_date and end_date:
            return session.query(PlacementReport)\
                .filter(and_(PlacementReport.DateTime >= start_date,PlacementReport.DateTime <= end_date))\
                .filter_by(**criteria)

        return session.query(PlacementReport)\
                .filter_by(**criteria)







